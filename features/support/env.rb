require 'bundler'
Bundler.require(:default)
require 'capybara/cucumber'
require 'capybara-screenshot/cucumber'


#--------------Global Config----------------
MAIN_CONFIG        = YAML.load_file("#{Dir.pwd}/config.yml")
BROWSER            = ENV['BROWSER'] || MAIN_CONFIG['browser']

#----------------Capybara-------------------

Capybara.default_max_wait_time      = 10
Capybara.default_selector           = :css
Capybara.default_driver             = :selenium
Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, :browser => BROWSER.to_sym)
end
Capybara.current_driver = :selenium
SitePrism.configure { |config| config.use_implicit_waits = true }