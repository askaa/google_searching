After do |scenario|
  Capybara.save_path = 'screenshots'
  Capybara::Screenshot.screenshot_and_save_page
  Capybara.reset_sessions!
end
