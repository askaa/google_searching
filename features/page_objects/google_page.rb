class GooglePage < SitePrism::Page

  set_url 'https://www.google.com.ua'
  element :search_input, '#lst-ib'
  element :search_button, '[name="btnK"]'
  elements :search_result_links, '.rc h3 a'

  def search(text)
    search_input.set text
    search_input.native.send_keys :enter
  end

  def open_link(name)
    wait_for_search_result_links
    search_result_links.select { |link| link[:href].include? name.downcase}.first.click
  end

  def open_next_link_after(name)
    wait_for_search_result_links
    search_result_links.reject { |link| link[:href].include? name.downcase}.first.click
  end


end