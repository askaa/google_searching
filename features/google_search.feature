Feature: As user I should find any word from google searching

  Scenario: Find in wiki
    Given I go to google search
    And I input "aikido" into searching
    When I open wikipedia link
    Then I should find this word in the page

  Scenario: Find in the next link
    Given I go to google search
    And I input "aikido" into searching
    When I open the next link
    Then I should find this word in the page