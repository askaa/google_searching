Given(/^I go to google search$/) do
  @current_page = GooglePage.new
  @current_page.load
end

And(/^I input "([^"]*)" into searching$/) do |text|
  @searching_text = text
  @current_page.search text
end

When(/^I open wikipedia link$/) do
  @current_page.open_link('wikipedia')
end

Then(/^I should find this word in the page$/) do
  expect(@current_page.title.downcase).to include(@searching_text)
end

When(/^I open the next link$/) do
  @current_page.open_next_link_after 'wikipedia'
end